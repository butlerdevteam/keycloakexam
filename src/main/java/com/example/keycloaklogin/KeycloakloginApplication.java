package com.example.keycloaklogin;

import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.adapters.spi.HttpFacade;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import java.io.InputStream;


@SpringBootApplication
public class KeycloakloginApplication {
    /**
     * Keycloak 설정 파일로 부터 설정 내용을 읽어 Resolver 객체를 생성한다.
     */
    private final KeycloakConfigResolver resolver = new KeycloakConfigResolver() {
        private KeycloakDeployment keycloakDeployment;
        @Override
        public KeycloakDeployment resolve(HttpFacade.Request facade) {
            if (keycloakDeployment != null) {
                return keycloakDeployment;
            }
            InputStream configInputStream = getClass().getResourceAsStream("/keycloak.json");
            return KeycloakDeploymentBuilder.build(configInputStream);
        }
    };

    /**
     * Keycloak 설정 내용을 담은 Resolver를 리턴한다.
     * 별도 설정이 없을 시 Keycloak Adapter는 WEB-INF 아래에서 keycloak.json 파일을 찾는다.
     * @return KeycloakConfigResolver
     */
    @Bean
    public KeycloakConfigResolver keycloakConfigResolver() {
        return resolver;
    }

    // HttpSessionEventPublisher 를 bean으로 등록하는 설정
    // Session 기반 인증시 분산환경에서 중복 세션 등록을 방지할 목적으로
    // WAS의 세션 이벤트를 Spring RootContext에서 수신하기 위해 등록하는 것이라 함.
    // access token 기반 인증으로 구현할 예정이기 때문에 주석 처리함.
//    @Bean
//    public ServletListenerRegistrationBean<HttpSessionEventPublisher> httpSessionEventPublisher() {
//        return new ServletListenerRegistrationBean<HttpSessionEventPublisher>(new HttpSessionEventPublisher());
//    }

    /**
     * Spring boot 실행 메서드
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(KeycloakloginApplication.class, args);
    }

}
