package com.example.keycloaklogin.controllers;

import org.keycloak.common.util.Base64;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * REST API 응답을 하는 Controller
 */
@RestController
public class Controller {

    /**
     * 어노테이션 기반 권한 설정. 아래 4가지가 있다.
     * @PreAuthorize: 메서드 실행 전 권한을 체크한다.
     * @PostAuthorize: 메서드를 실행하고 리턴 전 권한을 체크한다. 권한이 없으면 실행 값이 반환되지 않음
     * @PreFilter: 메서드 실행 전 조건에 맞지 않는 입력 값을 필터
     * @PostFilter: 메서드 실행 후 조건에 맞지 않는 리턴 값을 필터
     *
     * @RequestMapping: URL을 매핑하며 Class 단위에 공통 URL을 지정할 수 있다. 최근에는 @GetMapping, @PostMapping 등으로 대체되는 추세
     * @return
     */
    @PreAuthorize("hasRole('APT20USER')")
    @RequestMapping("/hello")
    public String hello() {
        return "hello";
    }
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping("/hello2")
    public String hello2() {
        return "hello2";
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping("/app")
    public String app(HttpServletRequest request) throws IOException {
        String auth = request.getHeader("Authentication");
        String decoded = Base64.decode(auth).toString();
        return "app";
    }

    @RequestMapping("/appsub")
    public String subapp() {
        return "subapp";
    }
}
